import 'dart:async';

import 'package:biometric_app/domain/routers/biometric_routes.dart';
import 'package:biometric_app/domain/usecases/auth_usecases.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'biometric_event.dart';
part 'biometric_state.dart';

class BiometricBloc extends Bloc<BiometricEvent, BiometricState> {

  AuthenticationUseCases _useCasesImplementation = AuthenticationUsecaseImpl();
  AuthRoute _authRoute = AuthRouteImpl();

  BiometricBloc() : super(BiometricInitial());

  @override
  Stream<BiometricState> mapEventToState(
    BiometricEvent event,
  ) async* {
    if(event is CheckDeviceSupportEvent) {
      await _useCasesImplementation.isBiometricEnabled().then((isSupported) async {
        if(isSupported) {
          await _useCasesImplementation.isAuthenticated().then((isAuthenticated) {
            if(isAuthenticated) {
              _authRoute.goToDashboard();
            }
          });
        }
      });
    }
  }
}
