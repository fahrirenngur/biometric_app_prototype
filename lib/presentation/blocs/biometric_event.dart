part of 'biometric_bloc.dart';

abstract class BiometricEvent extends Equatable{}

class CheckDeviceSupportEvent extends BiometricEvent {
  @override
  List<Object> get props => [];
}