part of 'biometric_bloc.dart';

abstract class BiometricState extends Equatable{}

class BiometricInitial extends BiometricState {

  @override
  List<Object> get props => [];

}

class AuthenticatedState extends BiometricState {
  final bool isAuthenticated;

  AuthenticatedState({this.isAuthenticated});
  @override
  List<Object> get props => [isAuthenticated];
}
