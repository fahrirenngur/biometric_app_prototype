import 'package:biometric_app/domain/usecases/auth_usecases.dart';
import 'package:biometric_app/presentation/blocs/biometric_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AuthenticationScreen extends StatefulWidget {
  @override
  _AuthenticationScreenState createState() => _AuthenticationScreenState();
}

class _AuthenticationScreenState extends State<AuthenticationScreen> {

  BiometricBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = BiometricBloc()..add(CheckDeviceSupportEvent());
  }

  @override
  void dispose() {
    super.dispose();
    _bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BiometricBloc>(
      create: (_) => Modular.get<BiometricBloc>(),
      child: Scaffold(
        body: BlocConsumer<BiometricBloc, BiometricState>(
          listener: (context, state) {

          },
          builder: (context, state) => Center(
            child: Text('Start Authenticating'),
          ),
        ),
      ),
    );
  }
}
