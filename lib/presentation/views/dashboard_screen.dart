import 'package:biometric_app/external/utils/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Dashboard Screen'), centerTitle: true,),
      body: Center(
        child: Text(Modular.get<AuthenticationStrings>().authenticationSuccessMessage),
      ),
    );
  }
}
