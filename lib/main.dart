import 'package:biometric_app/external/utils/route/routes_string.dart';
import 'package:biometric_app/data/repositories/auth_repositories_impl.dart';
import 'package:biometric_app/data/sources/data_sources.dart';
import 'package:biometric_app/domain/routers/biometric_routes.dart';
import 'package:biometric_app/domain/usecases/auth_usecases.dart';
import 'package:biometric_app/external/utils/app_strings.dart';
import 'package:biometric_app/presentation/blocs/biometric_bloc.dart';
import 'package:biometric_app/presentation/views/auth_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'presentation/views/dashboard_screen.dart';

void main() {
  runApp(ModularApp(
    module: AppModule(),
  ));
}

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((_) => BiometricRoutesString()),
        Bind((_) => AuthRouteImpl()),
        Bind((_) => DataSourceImpl()),
        Bind((_) => BiometricBloc()),
        Bind((_) => AuthenticationRepositoriesImpl()),
        Bind((_) => AuthenticationUsecaseImpl()),
        Bind((_) => AuthenticationStrings()),
      ];

  @override
  Widget get bootstrap => MyApp();

  @override
  List<ModularRouter> get routers => [
        ModularRouter(
          Modular.get<BiometricRoutesString>().auth,
          child: (context, args) => AuthenticationScreen(),
        ),
        ModularRouter(
          Modular.get<BiometricRoutesString>().dashboard,
          child: (_, args) => DashboardScreen(),
        ),
      ];
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Flutter App",
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: Modular.get<BiometricRoutesString>().auth,
      navigatorKey: Modular.navigatorKey,
      onGenerateRoute: Modular.generateRoute,
    );
  }
}
