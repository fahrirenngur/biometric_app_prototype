import 'package:biometric_app/data/sources/data_sources.dart';
import 'package:biometric_app/domain/repositories/auth_repositories.dart';

class AuthenticationRepositoriesImpl implements AuthenticationRepositories {

  final DataSources dataSource = DataSourceImpl();

  @override
  Future<bool> isAuthenticated() async =>
      await dataSource.isAuthenticated();

  @override
  Future<bool> isBiometricEnabled() async =>
      await dataSource.isBiometricEnabled();
}
