import 'package:biometric_app/external/utils/app_strings.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:local_auth/local_auth.dart';

abstract class DataSources {
  Future<bool> isAuthenticated();
  Future<bool> isBiometricEnabled();
}

class DataSourceImpl implements DataSources {

  final LocalAuthentication auth = LocalAuthentication();

  @override
  Future<bool> isAuthenticated() async {
    bool isAuthenticated = false;
    try {
      isAuthenticated = await auth.authenticateWithBiometrics(
          localizedReason: Modular.get<AuthenticationStrings>().dialogMessage,
          useErrorDialogs: true,
          stickyAuth: true);
    } on PlatformException catch (e) {
      print(e);
    }

    return isAuthenticated;
  }

  @override
  Future<bool> isBiometricEnabled() async {
    bool isEnabled = false;
    try {
      isEnabled = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }

    return isEnabled;
  }
}
