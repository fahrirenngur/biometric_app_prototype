abstract class AuthenticationRepositories {
  Future<bool> isBiometricEnabled();
  Future<bool> isAuthenticated();
}