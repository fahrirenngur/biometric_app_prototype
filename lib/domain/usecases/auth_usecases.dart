import 'package:biometric_app/data/repositories/auth_repositories_impl.dart';
import 'package:biometric_app/domain/repositories/auth_repositories.dart';

abstract class AuthenticationUseCases {
  Future<bool> isBiometricEnabled();
  Future<bool> isAuthenticated();
}

class AuthenticationUsecaseImpl implements AuthenticationUseCases {

  final AuthenticationRepositories repositories = AuthenticationRepositoriesImpl();

  @override
  Future<bool> isAuthenticated() async =>
    await repositories.isAuthenticated();

  @override
  Future<bool> isBiometricEnabled() async => await repositories.isBiometricEnabled();
}
