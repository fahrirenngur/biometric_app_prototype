import 'package:biometric_app/external/utils/route/routes_string.dart';
import 'package:flutter_modular/flutter_modular.dart';

abstract class AuthRoute {
  goToDashboard();
}

class AuthRouteImpl implements AuthRoute {

  @override
  goToDashboard() => Modular.to.pushReplacementNamed(Modular.get<BiometricRoutesString>().dashboard);

}